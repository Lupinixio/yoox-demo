package com.lupinixio.a20191014_cavalera_yooxdemo

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.rule.ActivityTestRule
import androidx.test.runner.AndroidJUnit4
import com.lupinixio.a20191014_cavalera_yooxdemo.ui_activity.MainActivity
import org.hamcrest.CoreMatchers.not
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class MainActivityEspressoTest {

    @JvmField
    @Rule
    val activityRule = ActivityTestRule<MainActivity>(MainActivity::class.java)

//    @Before
//    fun initialView() {
//        onView(withId(R.id.sortingRecyclerView)).check(matches(isDisplayed()))
//        onView(withId(R.id.resultListEmptyMessage)).check(matches(isDisplayed()))
//        onView(withId(R.id.progressBar)).check(matches(not(isDisplayed())))
//    }


    @Test
    fun initialViewDisplayedProperly() {
//        onView(withId(R.id.sortingRecyclerView)).check(matches(isDisplayed()))
//        onView(withId(R.id.resultListEmptyMessage)).check(matches(isDisplayed()))
        onView(withId(R.id.progressBar)).check(matches(not(isDisplayed())))
    }
}