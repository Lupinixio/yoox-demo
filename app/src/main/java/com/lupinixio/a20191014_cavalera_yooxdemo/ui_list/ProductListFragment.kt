package com.lupinixio.a20191014_cavalera_yooxdemo.ui_list

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.jakewharton.rxbinding2.widget.RxTextView
import com.lupinixio.a20191014_cavalera_yooxdemo.R
import com.lupinixio.a20191014_cavalera_yooxdemo.abstraction.dto.ProductDetail
import com.lupinixio.a20191014_cavalera_yooxdemo.abstraction.usecase.ListOrder
import com.lupinixio.a20191014_cavalera_yooxdemo.databinding.FragmentProductListBinding
import com.lupinixio.a20191014_cavalera_yooxdemo.ui_adapter.ProductAdapter
import com.lupinixio.a20191014_cavalera_yooxdemo.ui_adapter.SortingAdapter
import com.lupinixio.a20191014_cavalera_yooxdemo.ui_base.BaseFragment
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.concurrent.TimeUnit

class ProductListFragment : BaseFragment<FragmentProductListBinding>(),
    SwipeRefreshLayout.OnRefreshListener {

    private val vm: ProductListViewModel by viewModel()

    private val productClick: (ProductDetail) -> Unit = {
        navController.navigate(ProductListFragmentDirections.goToProductDetailPage(it.id))
    }
    private val sortingClick: (ListOrder) -> Unit = {
        vm.sortingOrder.postValue(it)
    }
    private val productAdapter by lazy { ProductAdapter(itemClick = productClick) }
    private val sortingAdapter by lazy {
        SortingAdapter(
            listOf(ListOrder.Lowest, ListOrder.Latest, ListOrder.Highest), sortingClick
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_product_list, container, false
        )
        binding.lifecycleOwner = this
        binding.swipeToRefresh.setOnRefreshListener(this)
        setVmCallback(vm)
        return binding.root
    }

    override fun onStart() {
        super.onStart()

        addDisposable(RxTextView.textChanges(binding.searchBox)
            .skipInitialValue()
            .map(CharSequence::toString)
            .debounce(250, TimeUnit.MILLISECONDS)
            .doOnNext { Log.d(TAG, "Search value -> $it") }
            .subscribeOn(Schedulers.computation())
            .subscribeBy(
                onNext = { vm.searchValue.postValue(it) },
                onError = {
                    Log.d(
                        TAG,
                        it.message ?: "SearchBox Observable, an error occurred"
                    )
                }
            )
        )
    }


    override fun setupInitialView() {
        binding.productRecyclerView.apply {
            layoutManager = LinearLayoutManager(context).apply { initialPrefetchItemCount = 10 }
            setHasFixedSize(true)
            adapter = productAdapter
        }

        binding.sortingRecyclerView.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            adapter = sortingAdapter
        }
    }

    override fun setupViewModelObservers() {
        vm.productList.observe(this, onProductListChange())
        vm.isListEmpty.observe(this, onListIsEmptyChange())
        vm.sortingOrder.observe(this, onSortingOrderChange())
        vm.isSwipeToRefreshVisible.observe(this, onSwipeToRefreshChange())
    }

    override fun onRefresh() {
        vm.refreshPage()
    }

    private fun onListIsEmptyChange(): Observer<in Boolean> {
        return Observer {
            binding.resultListEmptyMessage.visibility = if (it) View.VISIBLE else View.GONE
        }
    }

    private fun onProductListChange(): Observer<List<ProductDetail>> {
        return Observer {
            binding.productRecyclerView.smoothScrollToPosition(0)
            productAdapter.setNewList(it)
        }
    }

    private fun onSortingOrderChange(): Observer<in ListOrder> {
        return Observer {
            Log.d(TAG, "Sorting selection change -> $it")
            sortingAdapter.setSelectedItem(it)
        }
    }

    private fun onSwipeToRefreshChange(): Observer<in Boolean> {
        return Observer {
            binding.swipeToRefresh.isRefreshing = it
        }
    }
}