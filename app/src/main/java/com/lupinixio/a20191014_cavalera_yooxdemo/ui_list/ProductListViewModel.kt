package com.lupinixio.a20191014_cavalera_yooxdemo.ui_list

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.lupinixio.a20191014_cavalera_yooxdemo.abstraction.dto.ProductDetail
import com.lupinixio.a20191014_cavalera_yooxdemo.abstraction.usecase.ListOrder
import com.lupinixio.a20191014_cavalera_yooxdemo.abstraction.usecase.ProductUseCase
import com.lupinixio.a20191014_cavalera_yooxdemo.ui_base.BaseViewModel
import io.reactivex.rxkotlin.subscribeBy
import java.util.*

class ProductListViewModel(private val productUseCase: ProductUseCase) : BaseViewModel() {

    private val downloadedList =
        MutableLiveData<List<ProductDetail>>().apply { value = emptyList() }
    private val filteredList = MediatorLiveData<List<ProductDetail>>().apply { value = emptyList() }
    private val emptyList = MediatorLiveData<Boolean>().apply { value = false }
    private val swipeLoading = MutableLiveData<Boolean>()

    val searchValue = MutableLiveData<String>().apply { value = "" }
    val sortingOrder = MutableLiveData<ListOrder>().apply { value = ListOrder.Default }

    init {
        setupMediator()
        performSearch()
    }

    val productList: LiveData<List<ProductDetail>>
        get() = filteredList

    val isListEmpty: LiveData<Boolean>
        get() = emptyList

    val isSwipeToRefreshVisible: LiveData<Boolean>
        get() = swipeLoading

    fun refreshPage() {
        swipeLoading.value = true
        performSearch(sortingOrder.value ?: ListOrder.Default)
    }

    private fun setupMediator() {
        filteredList.addSource(searchValue, onSearchValueChange())
        filteredList.addSource(downloadedList, onDownloadedListChange())
        filteredList.addSource(sortingOrder, onSortingOrderChange())
        emptyList.addSource(filteredList, onFilteredListChange())
    }

    private fun performSearch(order: ListOrder = ListOrder.Default) {
        loadingInProgress.value = true
        addDisposable(productUseCase.getProducts(order)
            .subscribeBy(
                onNext = { downloadedList.postValue(it) },
                onError = {
                    error.postValue(it)
                    loadingInProgress.postValue(false)
                    swipeLoading.postValue(false)
                },
                onComplete = {
                    loadingInProgress.postValue(false)
                    swipeLoading.postValue(false)
                }
            ))
    }

    private fun onSearchValueChange(): Observer<in String> {
        return Observer {
            loadingInProgress.value = true
            downloadedList.value?.let { list ->
                filteredList.value = filterListBeforeUpdate(list, it)
            }
            loadingInProgress.value = false
        }
    }

    private fun onDownloadedListChange(): Observer<List<ProductDetail>> {
        return Observer {
            filteredList.value = filterListBeforeUpdate(it, searchValue.value ?: "")
        }
    }

    private fun onSortingOrderChange(): Observer<in ListOrder> {
        return Observer {
            performSearch(sortingOrder.value ?: ListOrder.Default)
        }
    }

    private fun onFilteredListChange(): Observer<List<ProductDetail>> {
        return Observer {
            emptyList.value = it.isEmpty()
        }
    }

    private fun filterListBeforeUpdate(
        list: List<ProductDetail>,
        filterValue: String
    ) = list.filter { propertiesToCheck(it, filterValue) }

    private fun propertiesToCheck(product: ProductDetail, filterValue: String): Boolean {
        val propertiesFilterable = listOf(product.brand, product.category)
        for (value in propertiesFilterable) {
            if (value.toLowerCase(Locale.getDefault()).contains(filterValue)) {
                return true
            }
        }
        return false
    }
}

