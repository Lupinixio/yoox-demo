package com.lupinixio.a20191014_cavalera_yooxdemo.domain

import com.lupinixio.a20191014_cavalera_yooxdemo.abstraction.dto.ProductDetail
import com.lupinixio.a20191014_cavalera_yooxdemo.abstraction.usecase.ListOrder
import com.lupinixio.a20191014_cavalera_yooxdemo.abstraction.usecase.ProductUseCase
import io.reactivex.Observable

class ProductRepositoryUseCase(private val productRepositoryPort: ProductRepositoryPort) :
    ProductUseCase {

    override fun getProducts(sortingOrder: ListOrder) = when (sortingOrder) {
        ListOrder.Default -> productRepositoryPort.getSearchResultDefault()
        ListOrder.Highest -> productRepositoryPort.getSearchResultSortedByHighest()
        ListOrder.Latest -> productRepositoryPort.getSearchResultSortedByLatest()
        ListOrder.Lowest -> productRepositoryPort.getSearchResultSortedByLowest()
    }

    override fun getProductDetail(): Observable<ProductDetail> {
        return productRepositoryPort.getProductDetail()
    }

}