package com.lupinixio.a20191014_cavalera_yooxdemo.domain

import com.lupinixio.a20191014_cavalera_yooxdemo.abstraction.dto.ProductDetail
import io.reactivex.Observable

interface ProductRepositoryPort {

    fun getSearchResultDefault(): Observable<List<ProductDetail>>

    fun getSearchResultSortedByLatest(): Observable<List<ProductDetail>>

    fun getSearchResultSortedByLowest(): Observable<List<ProductDetail>>

    fun getSearchResultSortedByHighest(): Observable<List<ProductDetail>>

    fun getProductDetail(): Observable<ProductDetail>

}