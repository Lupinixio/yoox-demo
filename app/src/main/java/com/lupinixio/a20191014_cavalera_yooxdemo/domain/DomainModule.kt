package com.lupinixio.a20191014_cavalera_yooxdemo.domain

import com.lupinixio.a20191014_cavalera_yooxdemo.abstraction.usecase.ProductUseCase
import com.lupinixio.a20191014_cavalera_yooxdemo.app.IModulesProvider
import org.koin.core.module.Module
import org.koin.dsl.module

object DomainModule : IModulesProvider {

    private val useCaseModules = module {
        single { ProductRepositoryUseCase(get()) as ProductUseCase }
    }

    override val modules: List<Module>
        get() = listOf(useCaseModules)

}