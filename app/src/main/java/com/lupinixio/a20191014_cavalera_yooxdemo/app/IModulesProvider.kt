package com.lupinixio.a20191014_cavalera_yooxdemo.app

import org.koin.core.module.Module

interface IModulesProvider {

    val modules: List<Module>
}