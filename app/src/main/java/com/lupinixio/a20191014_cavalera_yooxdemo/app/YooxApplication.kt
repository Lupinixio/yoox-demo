package com.lupinixio.a20191014_cavalera_yooxdemo.app

import android.app.Application
import android.content.Context
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class YooxApplication :Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            printLogger()
            androidContext(this@YooxApplication)
            modules(ModuleProvider.modules)
        }
    }

}