package com.lupinixio.a20191014_cavalera_yooxdemo.app

import android.content.ContentResolver
import com.lupinixio.a20191014_cavalera_yooxdemo.domain.DomainModule
import com.lupinixio.a20191014_cavalera_yooxdemo.repository.NetworkModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.module.Module
import org.koin.dsl.module

object ModuleProvider {

    private val appModules = module {
        single<ContentResolver> { androidContext().contentResolver }
    }

    val modules: List<Module>
        get() {
            return ArrayList<Module>().apply {
                add(appModules)
                addAll(DomainModule.modules)
                addAll(NetworkModule.modules)
            }
        }
}