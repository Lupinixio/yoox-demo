package com.lupinixio.a20191014_cavalera_yooxdemo.ui_adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Adapter.NO_SELECTION
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.lupinixio.a20191014_cavalera_yooxdemo.R
import com.lupinixio.a20191014_cavalera_yooxdemo.abstraction.dto.ProductColor
import com.lupinixio.a20191014_cavalera_yooxdemo.databinding.ItemColorBinding
import com.lupinixio.a20191014_cavalera_yooxdemo.ui_base.BaseViewHolder

class ColorAdapter(
    private val list: MutableList<ProductColor> = mutableListOf(),
    private val itemClick: (Int) -> Unit = {}
) : RecyclerView.Adapter<ColorAdapter.ColorViewHolder>() {

    private val TAG = javaClass.simpleName
    private var itemSelected = NO_SELECTION

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ColorViewHolder {
        val v =
            DataBindingUtil.inflate<ItemColorBinding>(
                LayoutInflater.from(parent.context),
                R.layout.item_color,
                parent,
                false
            )
        return ColorViewHolder(v)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ColorViewHolder, position: Int) {
        val currentItem = list[position]
        holder.bind(currentItem)
        holder.setSelected(position == itemSelected)

        holder.itemView.setOnClickListener {
            itemSelected = if (itemSelected == position) {
                NO_SELECTION
            } else {
                position
            }
            itemClick(itemSelected)
            notifyDataSetChanged()
        }
    }

    fun setNewList(items: List<ProductColor>) {
        itemSelected = NO_SELECTION
        list.clear()
        list.addAll(items)
        notifyDataSetChanged()
    }

    fun setSelectedItem(selected: Int) {
        itemSelected = selected
        notifyDataSetChanged()
    }

    inner class ColorViewHolder(binding: ItemColorBinding) :
        BaseViewHolder<ProductColor, ItemColorBinding>(binding) {

        fun setSelected(itemSelected: Boolean) {
            binding.colorName.apply {
                isSelected = itemSelected
                isAllCaps = itemSelected
            }
            binding.container.isSelected = itemSelected
        }
    }
}