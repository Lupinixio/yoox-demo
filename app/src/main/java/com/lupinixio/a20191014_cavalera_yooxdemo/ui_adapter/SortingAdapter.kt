package com.lupinixio.a20191014_cavalera_yooxdemo.ui_adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.lupinixio.a20191014_cavalera_yooxdemo.R
import com.lupinixio.a20191014_cavalera_yooxdemo.abstraction.usecase.ListOrder
import com.lupinixio.a20191014_cavalera_yooxdemo.databinding.ItemSortingBinding
import com.lupinixio.a20191014_cavalera_yooxdemo.ui_base.BaseViewHolder


class SortingAdapter(
    private val list: List<ListOrder>,
    private val itemClick: (ListOrder) -> Unit = {}
) : RecyclerView.Adapter<SortingAdapter.SortingViewHolder>() {
    private val NO_SELECTION = ListOrder.Default
    private var itemSelected: ListOrder = NO_SELECTION

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SortingViewHolder {
        val v =
            DataBindingUtil.inflate<ItemSortingBinding>(
                LayoutInflater.from(parent.context),
                R.layout.item_sorting,
                parent,
                false
            )
        return SortingViewHolder(v)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: SortingViewHolder, position: Int) {
        val currentItem = list[position]
        holder.bind(currentItem)

        holder.setSelected(currentItem == itemSelected)

        holder.itemView.setOnClickListener {
            itemSelected = if (itemSelected == currentItem) {
                NO_SELECTION
            } else {
                currentItem
            }

            itemClick(itemSelected)
            notifyDataSetChanged()
        }
    }

    fun setSelectedItem(selected: ListOrder) {
        itemSelected = selected
        notifyDataSetChanged()
    }

    inner class SortingViewHolder(binding: ItemSortingBinding) :
        BaseViewHolder<ListOrder, ItemSortingBinding>(binding) {

        fun setSelected(itemSelected: Boolean) {
            binding.sortingOrder.isSelected = itemSelected
        }
    }
}