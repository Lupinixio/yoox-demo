package com.lupinixio.a20191014_cavalera_yooxdemo.ui_adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Adapter.NO_SELECTION
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.lupinixio.a20191014_cavalera_yooxdemo.R
import com.lupinixio.a20191014_cavalera_yooxdemo.databinding.ItemSizeBinding
import com.lupinixio.a20191014_cavalera_yooxdemo.ui_base.BaseViewHolder


class SizeAdapter(
    private val list: MutableList<String> = mutableListOf(),
    private val itemClick: (Int) -> Unit = {}
) : RecyclerView.Adapter<SizeAdapter.SizeViewHolder>() {

    private val TAG = javaClass.simpleName
    private var itemSelected = NO_SELECTION

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SizeViewHolder {
        val v =
            DataBindingUtil.inflate<ItemSizeBinding>(
                LayoutInflater.from(parent.context),
                R.layout.item_size,
                parent,
                false
            )
        return SizeViewHolder(v)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: SizeViewHolder, position: Int) {
        val currentItem = list[position]
        holder.bind(currentItem)
        holder.setSelected(position == itemSelected)

        holder.itemView.setOnClickListener {
            itemSelected = if (itemSelected == position) {
                NO_SELECTION
            } else {
                position
            }
            itemClick(itemSelected)
            notifyDataSetChanged()
        }
    }

    fun setNewList(items: List<String>) {
        itemSelected = NO_SELECTION
        list.clear()
        list.addAll(items)
        notifyDataSetChanged()
    }

    fun setSelectedItem(selected: Int) {
        itemSelected = selected
        notifyDataSetChanged()
    }

    inner class SizeViewHolder(binding: ItemSizeBinding) :
        BaseViewHolder<String, ItemSizeBinding>(binding) {

        fun setSelected(itemSelected: Boolean) {
            binding.sizeLabel.isSelected = itemSelected
        }
    }
}