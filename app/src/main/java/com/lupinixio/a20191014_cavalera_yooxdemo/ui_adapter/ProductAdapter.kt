package com.lupinixio.a20191014_cavalera_yooxdemo.ui_adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.lupinixio.a20191014_cavalera_yooxdemo.R
import com.lupinixio.a20191014_cavalera_yooxdemo.abstraction.dto.ProductDetail
import com.lupinixio.a20191014_cavalera_yooxdemo.databinding.ItemProductBinding
import com.lupinixio.a20191014_cavalera_yooxdemo.ui_base.BaseViewHolder
import com.squareup.picasso.Picasso

class ProductAdapter(
    private val list: MutableList<ProductDetail> = mutableListOf(),
    private val itemClick: (ProductDetail) -> Unit = {}
) :
    RecyclerView.Adapter<ProductAdapter.ProductViewHolder>() {

    private val TAG = javaClass.simpleName

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
        val v =
            DataBindingUtil.inflate<ItemProductBinding>(
                LayoutInflater.from(parent.context),
                R.layout.item_product,
                parent,
                false
            )
        return ProductViewHolder(v)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
        val currentItem = list[position]
        holder.bind(currentItem)
    }

    fun setNewList(items: List<ProductDetail>) {
        list.clear()
        list.addAll(items)
        notifyDataSetChanged()
    }

    inner class ProductViewHolder(binding: ItemProductBinding) :
        BaseViewHolder<ProductDetail, ItemProductBinding>(binding) {

        override fun bind(obj: ProductDetail) {
            super.bind(obj)
            itemView.setOnClickListener { itemClick(obj) }
        }
    }
}

