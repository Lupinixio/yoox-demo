package com.lupinixio.a20191014_cavalera_yooxdemo.abstraction.dto

interface ProductColor {
    val name: String
    val rgb: Int
}