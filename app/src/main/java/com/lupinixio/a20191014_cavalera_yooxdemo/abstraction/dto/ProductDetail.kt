package com.lupinixio.a20191014_cavalera_yooxdemo.abstraction.dto

interface ProductDetail {
    val id: String
    val brand: String
    val category: String
    val price: String
    val imageUrl: String
    val info: List<String>
    val colors: List<ProductColor>
    val sizes: List<String>
}