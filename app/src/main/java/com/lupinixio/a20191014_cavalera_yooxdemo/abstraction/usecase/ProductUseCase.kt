package com.lupinixio.a20191014_cavalera_yooxdemo.abstraction.usecase

import com.lupinixio.a20191014_cavalera_yooxdemo.R
import com.lupinixio.a20191014_cavalera_yooxdemo.abstraction.dto.ProductDetail
import io.reactivex.Observable

interface ProductUseCase {

    fun getProducts(sortingOrder: ListOrder = ListOrder.Default): Observable<List<ProductDetail>>

    fun getProductDetail(): Observable<ProductDetail>

}

enum class ListOrder(val textResource: Int) {
    Default(R.string.default_sort),
    Lowest(R.string.lowest_sort),
    Latest(R.string.latest_sort),
    Highest(R.string.highest_sort)
}