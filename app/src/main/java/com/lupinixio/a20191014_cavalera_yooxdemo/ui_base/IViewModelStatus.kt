package com.lupinixio.a20191014_cavalera_yooxdemo.ui_base

import androidx.lifecycle.MutableLiveData

interface IViewModelStatus {
    val error: MutableLiveData<Throwable>
    val loadingInProgress: MutableLiveData<Boolean>
    val toastMessage: MutableLiveData<String>
}