package com.lupinixio.a20191014_cavalera_yooxdemo.ui_base

import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import com.squareup.picasso.Picasso

@BindingAdapter(value = ["textFromResources"])
fun TextView.setTextFromResources(resourceId: Int) {
    text = context.resources.getString(resourceId)
}

@BindingAdapter(value = ["textFromList"])
fun TextView.setTextFromList(list: List<String>? = emptyList()) {
    text = list?.joinToString(separator = "\n\n")
}

@BindingAdapter(value = ["imageFromURL"])
fun ImageView.setImageFromURL(value: String?) {
    Picasso.get().load(value).into(this)
}

