package com.lupinixio.a20191014_cavalera_yooxdemo.ui_base

interface IActivityStatus {
    fun isLoading(isLoading: Boolean)
}