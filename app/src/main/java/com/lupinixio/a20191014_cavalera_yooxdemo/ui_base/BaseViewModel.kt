package com.lupinixio.a20191014_cavalera_yooxdemo.ui_base

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

abstract class BaseViewModel : ViewModel(), IViewModelStatus {

    override val error = MutableLiveData<Throwable>()
    override val loadingInProgress = MutableLiveData<Boolean>()
    override val toastMessage = MutableLiveData<String>()
    private val compositeDisposable = CompositeDisposable()

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }

    protected fun addDisposable(disposable: Disposable) {
        compositeDisposable.add(disposable)
    }

    protected fun clearAllDisposable() {
        compositeDisposable.clear()
    }
}