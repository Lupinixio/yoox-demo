package com.lupinixio.a20191014_cavalera_yooxdemo.ui_activity

import com.lupinixio.a20191014_cavalera_yooxdemo.ui_base.UiModules
import com.lupinixio.a20191014_cavalera_yooxdemo.ui_details.ProductDetailViewModel
import com.lupinixio.a20191014_cavalera_yooxdemo.ui_list.ProductListViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.context.loadKoinModules
import org.koin.core.module.Module
import org.koin.dsl.module

object MainActivityModules : UiModules {
    override fun load() = loadMainActivityModules

    private val loadMainActivityModules by lazy {
        loadKoinModules(
            listOf(
                viewModelModules
            )
        )
    }

    private val viewModelModules: Module = module {
        viewModel { ProductListViewModel(get()) }
        viewModel { (id: String) -> ProductDetailViewModel(get(), id) }
    }

}