package com.lupinixio.a20191014_cavalera_yooxdemo.repository.api

import com.google.gson.annotations.SerializedName

data class PriceObjAPI(
    @SerializedName("FullPrice")
    val fullPrice: String = "",
    @SerializedName("DiscountedPrice")
    val discountPrice: String = ""
)