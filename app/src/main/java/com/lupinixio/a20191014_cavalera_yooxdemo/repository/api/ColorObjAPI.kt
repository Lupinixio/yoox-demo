package com.lupinixio.a20191014_cavalera_yooxdemo.repository.api

import android.graphics.Color
import com.google.gson.annotations.SerializedName
import com.lupinixio.a20191014_cavalera_yooxdemo.abstraction.dto.ProductColor

data class ColorObjAPI(
    @SerializedName("ColorId")
    val id: Int = 0, @SerializedName("Code10")
    val code: String = "",
    @SerializedName("Rgb")
    val rgbString: String = "",
    @SerializedName("ColorCode")
    val colorCode: String = "",
    @SerializedName("Name")
    override val name: String = ""
) : ProductColor{

    override val rgb: Int
        get() = Color.parseColor("#$rgbString")
}