package com.lupinixio.a20191014_cavalera_yooxdemo.repository


import com.lupinixio.a20191014_cavalera_yooxdemo.repository.api.ProductDetailAPI
import com.lupinixio.a20191014_cavalera_yooxdemo.repository.api.SearchResultAPI
import io.reactivex.Single
import retrofit2.http.GET

interface IYooxApiClient {

    @GET("searchresult")
    fun getSearchResult(): Single<SearchResultAPI>

    @GET("latest")
    fun getSearchResultLatest(): Single<SearchResultAPI>

    @GET("lowest")
    fun getSearchResultLowest(): Single<SearchResultAPI>

    @GET("highest")
    fun getSearchResultHighest(): Single<SearchResultAPI>

    @GET("item")
    fun getProductDetail():Single<ProductDetailAPI>
}