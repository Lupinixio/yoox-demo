package com.lupinixio.a20191014_cavalera_yooxdemo.repository

import com.lupinixio.a20191014_cavalera_yooxdemo.app.IModulesProvider
import com.lupinixio.a20191014_cavalera_yooxdemo.domain.ProductRepositoryPort
import org.koin.core.module.Module
import org.koin.dsl.module

object NetworkModule : IModulesProvider {


    private val apiModules = module {
        single { YooxApi as IYooxApiClient }
    }

    private val portModules = module {
        single { ProductApiAdapter(get()) as ProductRepositoryPort }
    }

    override val modules: List<Module>
        get() = listOf(apiModules, portModules)
}