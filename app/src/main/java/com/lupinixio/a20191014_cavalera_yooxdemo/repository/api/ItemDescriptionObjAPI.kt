package com.lupinixio.a20191014_cavalera_yooxdemo.repository.api

import com.google.gson.annotations.SerializedName

data class ItemDescriptionObjAPI(
    @SerializedName("ProductInfo")
    val info: List<String> = emptyList()
)