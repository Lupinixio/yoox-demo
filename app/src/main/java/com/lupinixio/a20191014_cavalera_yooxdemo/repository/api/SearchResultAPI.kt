package com.lupinixio.a20191014_cavalera_yooxdemo.repository.api

import com.google.gson.annotations.SerializedName

data class SearchResultAPI(
    @SerializedName("SearchResultTitle")
    val searchResultTitle: String = "",
    @SerializedName("Items")
    val products: List<ProductSearchResultAPI> = emptyList()
)