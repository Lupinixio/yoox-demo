package com.lupinixio.a20191014_cavalera_yooxdemo.repository.api

import com.google.gson.annotations.SerializedName

data class SizeObjAPI(
    @SerializedName("AlternativeSizeLabel")
    val alternativeSizeLabel: String = "",
    @SerializedName("DefaultSizeLabel")
    val defaultSizeLabel: String = "",
    @SerializedName("Id")
    val id: Int = 0,
    @SerializedName("IsoTwoLetterCountryCode")
    val isoTwoLetterCountryCode: String = "",
    @SerializedName("Name")
    val name: String = ""
)