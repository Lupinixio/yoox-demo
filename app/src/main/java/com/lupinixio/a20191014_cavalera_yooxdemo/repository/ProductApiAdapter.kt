package com.lupinixio.a20191014_cavalera_yooxdemo.repository

import com.lupinixio.a20191014_cavalera_yooxdemo.abstraction.dto.ProductDetail
import com.lupinixio.a20191014_cavalera_yooxdemo.domain.ProductRepositoryPort
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers

class ProductApiAdapter(private val productClient: IYooxApiClient) :
    ProductRepositoryPort {
    override fun getSearchResultDefault(): Observable<List<ProductDetail>> {
        return productClient.getSearchResult()
            .map {
                it.products.map { item -> item as ProductDetail }
            }.toObservable().subscribeOn(Schedulers.io())
    }

    override fun getSearchResultSortedByLatest(): Observable<List<ProductDetail>> {
        return productClient.getSearchResultLatest()
            .map {
                it.products.map { item -> item as ProductDetail }
            }.toObservable().subscribeOn(Schedulers.io())
    }

    override fun getSearchResultSortedByLowest(): Observable<List<ProductDetail>> {
        return productClient.getSearchResultLowest()
            .map {
                it.products.map { item -> item as ProductDetail }
            }.toObservable().subscribeOn(Schedulers.io())
    }

    override fun getSearchResultSortedByHighest(): Observable<List<ProductDetail>> {
        return productClient.getSearchResultHighest()
            .map {
                it.products.map { item -> item as ProductDetail }
            }.toObservable().subscribeOn(Schedulers.io())
    }

    override fun getProductDetail(): Observable<ProductDetail> {
        return productClient.getProductDetail()
            .map { it as ProductDetail }
            .toObservable().subscribeOn(Schedulers.io())
    }

}