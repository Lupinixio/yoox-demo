package com.lupinixio.a20191014_cavalera_yooxdemo.repository.api

import com.google.gson.annotations.SerializedName

data class BrandObjAPI(
    @SerializedName("Name")
    val name: String = ""
)