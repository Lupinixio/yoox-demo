package com.lupinixio.a20191014_cavalera_yooxdemo.repository.api

import com.google.gson.annotations.SerializedName
import com.lupinixio.a20191014_cavalera_yooxdemo.abstraction.dto.ProductColor
import com.lupinixio.a20191014_cavalera_yooxdemo.abstraction.dto.ProductDetail

data class ProductSearchResultAPI(
    @SerializedName("Cod10")
    val cod: String = "",
    @SerializedName("Brand")
    override val brand: String = "",
    @SerializedName("MicroCategory")
    override val category: String = "",
    @SerializedName("FormattedFullPrice")
    val formattedFullPrice: String = "",
    @SerializedName("FormattedDiscountedPrice")
    val formattedDiscountedPrice: String = ""
) : ProductDetail {
    override val id: String
        get() = cod

    override val price: String
        get() = if (formattedDiscountedPrice == formattedFullPrice) {
            formattedFullPrice
        } else {
            formattedDiscountedPrice
        }

    override val imageUrl: String
        get() = "https://cdn.yoox.biz/${cod.subSequence(0, 2)}/${cod}_11_f.jpg"

    override val colors: List<ProductColor>
        get() = emptyList()

    override val info: List<String>
        get() = emptyList()

    override val sizes: List<String>
        get() = emptyList()
}