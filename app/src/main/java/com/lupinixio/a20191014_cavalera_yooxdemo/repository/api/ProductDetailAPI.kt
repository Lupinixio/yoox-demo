package com.lupinixio.a20191014_cavalera_yooxdemo.repository.api

import com.google.gson.annotations.SerializedName
import com.lupinixio.a20191014_cavalera_yooxdemo.abstraction.dto.ProductColor
import com.lupinixio.a20191014_cavalera_yooxdemo.abstraction.dto.ProductDetail

data class ProductDetailAPI(
    @SerializedName("Cod10")
    val cod: String = "",
    @SerializedName("Brand")
    val brandObj: BrandObjAPI,
    @SerializedName("Category")
    val categoryObj: CategoryObjAPI,
    @SerializedName("FormattedPrice")
    val priceObj: PriceObjAPI,
    @SerializedName("ItemDescriptions")
    val descriptionObj: ItemDescriptionObjAPI,
    @SerializedName("Colors")
    val colorsObj: List<ColorObjAPI>,
    @SerializedName("Sizes")
    val sizesObj: List<SizeObjAPI>
) : ProductDetail {
    override val id: String
        get() = cod

    override val brand: String
        get() = brandObj.name

    override val category: String
        get() = categoryObj.name

    override val colors: List<ProductColor>
        get() = colorsObj.map { it as ProductColor }

    override val imageUrl: String
        get() = "https://cdn.yoox.biz/${cod.subSequence(0, 2)}/${cod}_11_f.jpg"

    override val info: List<String>
        get() = descriptionObj.info

    override val price: String
        get() = if (priceObj.fullPrice == priceObj.discountPrice) {
            priceObj.fullPrice
        } else {
            priceObj.discountPrice
        }

    override val sizes: List<String>
        get() = sizesObj.map { it.defaultSizeLabel }
}