package com.lupinixio.a20191014_cavalera_yooxdemo.repository

import com.lupinixio.a20191014_cavalera_yooxdemo.repository.api.ProductDetailAPI
import com.lupinixio.a20191014_cavalera_yooxdemo.repository.api.SearchResultAPI
import io.reactivex.Single
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

object YooxApi : IYooxApiClient {

    private val TAG = javaClass.simpleName
    private const val base_url = "http://5aaf9b98bcad130014eeaf0b.mockapi.io/ynap/v1/"

    private val onHttpClient: OkHttpClient by lazy { OkHttpClient().newBuilder().build() }
    private val retrofit: Retrofit by lazy {
        Retrofit.Builder()
            .baseUrl(base_url)
            .client(onHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
    }

    private val client = retrofit.create(IYooxApiClient::class.java)

    override fun getSearchResult(): Single<SearchResultAPI> {
        return client.getSearchResult()
    }

    override fun getSearchResultLatest(): Single<SearchResultAPI> {
        return client.getSearchResultLatest()
    }

    override fun getSearchResultLowest(): Single<SearchResultAPI> {
        return client.getSearchResultLowest()
    }

    override fun getSearchResultHighest(): Single<SearchResultAPI> {
        return client.getSearchResultHighest()
    }

    override fun getProductDetail(): Single<ProductDetailAPI> {
        return client.getProductDetail()
    }
}