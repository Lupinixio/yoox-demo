package com.lupinixio.a20191014_cavalera_yooxdemo.ui_details

import android.util.Log
import android.widget.Adapter.NO_SELECTION
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.lupinixio.a20191014_cavalera_yooxdemo.abstraction.dto.ProductColor
import com.lupinixio.a20191014_cavalera_yooxdemo.abstraction.dto.ProductDetail
import com.lupinixio.a20191014_cavalera_yooxdemo.abstraction.usecase.ProductUseCase
import com.lupinixio.a20191014_cavalera_yooxdemo.ui_base.BaseViewModel
import io.reactivex.rxkotlin.subscribeBy

class ProductDetailViewModel(private val productUseCase: ProductUseCase, productId: String) :
    BaseViewModel() {

    private val TAG = javaClass.simpleName

    private val productId = MutableLiveData<String>().apply { value = productId }
    private val product = MediatorLiveData<ProductDetail>()
    private val colorList = MediatorLiveData<List<ProductColor>>()
    private val sizeList = MediatorLiveData<List<String>>()

    val selectedColor = MediatorLiveData<Int>()
    val selectedSize = MediatorLiveData<Int>()

    init {
        setupMediator()
    }

    val productDetail: LiveData<ProductDetail>
        get() = product

    val productColors: LiveData<List<ProductColor>>
        get() = colorList

    val productSize: LiveData<List<String>>
        get() = sizeList

    private fun setupMediator() {
        product.addSource(productId, onProductIdChange())
        colorList.addSource(product, onProductChangeUpdateColor())
        sizeList.addSource(product, onProductChangeUpdateSize())
    }

    private fun onProductIdChange(): Observer<in String> {
        return Observer {
            Log.d(TAG, "ProductId changed")
            loadingInProgress.value = true
            productUseCase.getProductDetail()
                .subscribeBy(
                    onNext = { product.postValue(it) },
                    onError = {
                        error.postValue(it)
                        loadingInProgress.postValue(false)
                    },
                    onComplete = { loadingInProgress.postValue(false) }
                )
        }
    }

    private fun onProductChangeUpdateColor(): Observer<in ProductDetail> {
        return Observer {
            colorList.value = it.colors
        }
    }

    private fun onProductChangeUpdateSize(): Observer<in ProductDetail> {
        return Observer {
            sizeList.value = it.sizes
        }
    }
}