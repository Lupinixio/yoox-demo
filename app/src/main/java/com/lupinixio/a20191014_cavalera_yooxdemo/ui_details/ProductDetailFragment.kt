package com.lupinixio.a20191014_cavalera_yooxdemo.ui_details

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.lupinixio.a20191014_cavalera_yooxdemo.R
import com.lupinixio.a20191014_cavalera_yooxdemo.abstraction.dto.ProductColor
import com.lupinixio.a20191014_cavalera_yooxdemo.databinding.FragmentProductDetailBinding
import com.lupinixio.a20191014_cavalera_yooxdemo.ui_adapter.ColorAdapter
import com.lupinixio.a20191014_cavalera_yooxdemo.ui_adapter.SizeAdapter
import com.lupinixio.a20191014_cavalera_yooxdemo.ui_base.BaseFragment
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class ProductDetailFragment : BaseFragment<FragmentProductDetailBinding>() {

    private val args: ProductDetailFragmentArgs by navArgs()
    private val vm: ProductDetailViewModel by viewModel { parametersOf(args.id) }

    private val colorClick: (Int) -> Unit = {
        Log.d(TAG, "Click on color at position -> $it")
        vm.selectedColor.postValue(it)
    }
    private val sizeClick: (Int) -> Unit = {
        Log.d(TAG, "Click on size at position -> $it")
        vm.selectedSize.postValue(it)
    }

    private val colorAdapter by lazy { ColorAdapter(itemClick = colorClick) }
    private val sizeAdapter by lazy { SizeAdapter(itemClick = sizeClick) }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_product_detail, container, false
        )

        binding.lifecycleOwner = this
        binding.model = vm
        setVmCallback(vm)
        return binding.root
    }

    override fun setupInitialView() {
        binding.colorRecyclerView.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            adapter = colorAdapter
        }

        binding.sizeRecyclerView.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            adapter = sizeAdapter
        }
    }

    override fun setupViewModelObservers() {
        vm.productColors.observe(this, onColorListChange())
        vm.productSize.observe(this, onProductSizeChange())
        vm.selectedColor.observe(this, onColorSelectionChange())
        vm.selectedSize.observe(this, onSizeSelectedChange())
    }

    private fun onColorListChange(): Observer<List<ProductColor>> {
        return Observer {
            Log.d(TAG, "Color list change -> $it")
            colorAdapter.setNewList(it)
        }
    }

    private fun onProductSizeChange(): Observer<List<String>> {
        return Observer {
            Log.d(TAG, "Size list change -> $it")
            sizeAdapter.setNewList(it)
        }
    }

    private fun onColorSelectionChange(): Observer<in Int> {
        return Observer {
            Log.d(TAG, "Color selection change -> $it")
            colorAdapter.setSelectedItem(it)
        }
    }

    private fun onSizeSelectedChange(): Observer<in Int> {
        return Observer {
            Log.d(TAG, "Size selection change -> $it")
            sizeAdapter.setSelectedItem(it)
        }
    }


}