Cavalera Leonardo YooxDemo App


Demo project:
- Hexagonal Clean Architecture approach
- Dependency Injection with Koin
- Jetpack Navigation
- Dark mode
- Easy to RUDT (read, update, debug & test)
- [koin (Kotlin Dependency Injection framework)](https://github.com/InsertKoinIO/koin)


Project info
-
From some time I was experimenting with new Architecture patterns, looking for one easy to maintain and to implement.
In Semptember I found the article linked below about Hexagonal Architecture and I started to implement it on all my new projects (for now only personal project and demo project).
I think this architecture is what I was looking for.

For this specific project I didn't create separate modules for each component and I didn't implement the persistence module.
I imported even some base class I use in my projects.
I tried to follow your indication for the project as best I can.
Usually, for the projects I worked on we didn't implement UnitTest or Espresso Test, and the only things I know are about some guides I read. For these reasons I decided not to delivery something I'm not confident about.

I used RxJava since my first Android App and I'm continuing to use it for the logic part of the app because I found its operators more complete for now. I use LiveData for communication between ViewModels and Fragments.

I implemented a basic dark and light mode and a clean UI trying to follow material design pattern. For the detail page, I decided to have a different layout for portrait and landscape mode. I didn't handle tablet UI becouse I think it is out of the scope of this demo.

Obviously there is always something to do better, but I think that beyond mistakes I could make in this project, you could have a clear view of my coding skills.


Articles
-
[![Android Modularization: Hexagonal Architecture with Kotlin and MVVM (Peyman Arab)](https://medium.com/@mjpeyman.arab/android-modularization-hexagonal-architecture-with-kotlin-and-mvvm-part-1-db338833d6c2)]

License
-

    Copyright 2019 Leonardo Cavalera

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
